from PIL import Image
import numpy as np
import os


def get_size(imagesDirectory, filesList, percentile0=90, percentile1=90):
    alldim0 = [Image.open(os.path.join(imagesDirectory, index)).size[0] for index in filesList]
    alldim1 = [Image.open(os.path.join(imagesDirectory, index)).size[1] for index in filesList]

    # 90% percentiles of each dimension on the set
    #TODO : gridsearch on percentiles
    dim0 = int(np.percentile(alldim0, percentile0))
    dim1 = int(np.percentile(alldim1, percentile1))
    size = (dim0, dim1)
    return size


#Pour sélectionner une partie des données aléatoirement *​ (edited)
def selectData(filesList, nbImages = 1000):
    randomImages = np.random.choice(filesList, nbImages, replace=False)
    return randomImages

#Pour mettre en noir et blanc*​
def color_to_black(image):
    newimage = image.convert('L')
    return(newimage)

#Augmenter le contraste
def higher_contrast(image, contrastlevel = 5):
    ar_image = contrastlevel*(np.array(image.getdata())-np.mean(np.array(image.getdata())))
    return ar_image

#Takes too long madafaka
def img_to_matrixRGB(filename, standard_size = (120,120),verbose=False):
    """
    takes a filename and turns it into a numpy array of RGB pixels
    """
    img = Image.open(filename)
    if verbose:
        print("changing size from %s to %s" % (str(img.size), str(standard_size)))
    img = img.resize(standard_size)
    img = list(img.getdata())
    img = map(list, img)
    img = np.array(img)
    return img

def flatten_image(img):
    """
    takes in an (m, n) numpy array and flattens it
    into an array of shape (1, m * n)
    -> one image, one row
    """
    s = img.shape[0] * img.shape[1]
    img_wide = img.reshape(1, s)
    return img_wide[0]

