from in_settings import *
import numpy as np
import cv2
from matplotlib import pyplot as plt
import os
import sklearn

grabcut = False
clustering = False


if grabcut:
    for f in os.listdir(imagesDirectory):
        print(f)
        img = cv2.imread(os.path.join(imagesDirectory,  f))
        mask = np.zeros(img.shape[:2], np.uint8)
        for i in range(img.shape[0]):
            condition1 = i<0.1*img.shape[0]
            condition2 = i>0.9*img.shape[0]
            for j in range(img.shape[1]):
                condition3 = j < 0.1 * img.shape[1]
                condition4 = j > 0.9 * img.shape[1]
                if condition1 | condition2 | condition3 | condition4:
                    mask[i, j] = 2
                else:
                    mask[i, j] = 3


        bgdModel = np.zeros((1, 65), np.float64)
        fgdModel = np.zeros((1, 65), np.float64)

        rect = (0, 0, img.shape[0], img.shape[1])
        cv2.grabCut(img, mask, rect, bgdModel, fgdModel, 5  , cv2.GC_INIT_WITH_MASK)

        mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
        img = img*mask2[:, :, np.newaxis]

        # plt.imshow(img), plt.colorbar(), plt.show(block=False)

        cv2.imwrite(os.path.join(roof_only, f), img)




