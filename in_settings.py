import os
import sys

BASE_DIR = os.path.dirname(__file__)
sys.path.append(BASE_DIR)
LOCALALEX = (os.environ['USER'] == 'alexandre')


if LOCALALEX:
    roof_only = "/home/alexandre/Documents/datasciencegame/roof_only"
    directory = "/home/alexandre/Documents/datasciencegame/thon1"
    imagesDirectory = "/home/alexandre/Documents/datasciencegame/roof_images/"
    pathTrain = "/home/alexandre/Documents/datasciencegame/id_train.csv"

