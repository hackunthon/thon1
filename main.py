import ImageTreatment
import os
import numpy as np
import pandas as pd
from PIL import Image
import FeatureExtraction
import Classifications
from in_settings import *

# directory = "C:\\Users\\lamjo_000\\Google Drive\\Datasciencegame"
# imagesDirectory = os.path.join(directory, "roof_images")
idTrain = pd.read_csv(pathTrain)
idTrain.index = idTrain.pop('Id')
filesList = [f for f in imagesDirectory if (os.path.isfile(os.path.join(imagesDirectory, f)) and
                                                                    (int(f.split(".")[0]) in idTrain.index))]
#TODO : gridsearch percentiles
size = ImageTreatment.get_size(imagesDirectory, filesList)

#TODO : more data
randomImages = ImageTreatment.selectData(filesList)

imagesArray = []
for f in randomImages:
   image = Image.open(os.path.join(imagesDirectory,f))
   #Resize and color to black
   image = ImageTreatment.color_to_black(image.resize(size))
   #TODO gridsearch on level of contrast
   imagesArray.append(ImageTreatment.higher_contrast(image))
print("Image processing : Done !")
imagesArray = np.array(imagesArray)


#​*Et pour la partie Scikit Learn*​
#Get id
idloc = [int(im.split(".")[0]) for im in randomImages]

y=np.ravel(idTrain.loc[idloc])
print(y.shape)

X=imagesArray
print(X.shape)
